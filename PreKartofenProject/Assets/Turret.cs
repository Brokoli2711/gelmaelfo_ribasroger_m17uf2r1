using System.Collections;
using UnityEngine;

public class TurretEnemy : Enemy
{
    public GameObject bulletPrefab;

    void Start()
    {
        StartCoroutine(FireRoutine());
    }

    IEnumerator FireRoutine()
    {
        while (true)
        {
            RangedAttack();
            yield return new WaitForSeconds(3f); 
        }
    }

    void RangedAttack()
    {
        Shoot(Vector2.up);

        Shoot(Vector2.down);

        Shoot(Vector2.right);

        Shoot(Vector2.left);
    }

    void Shoot(Vector2 direction)
    {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        Rigidbody2D bulletRb = bullet.GetComponent<Rigidbody2D>();

        bulletRb.velocity = direction * 5f; 

        Destroy(bullet, 2f); 
    }
}