using System.Collections;
using UnityEngine;

public class Targeter : Enemy, IMovement
{
    [SerializeField] public float speed;
    private Transform playerTransform;
    private bool moving;

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform; // Hola carlos, si lees esto es porque he decidido no tirarme por el balcon (hay que probar silksong antes)
        this.rb2d = GetComponent<Rigidbody2D>();
        this._anim = GetComponent<Animator>();
        moving = false;
    }

    void Update()
    {
        MoveTowardsPlayer();
    }

    public void Move()
    {
        // Hasta los cojones del roguelike
    }

    private void MoveTowardsPlayer()
    {
        if (!moving)
        {
            Vector2 playerPosition = playerTransform.position;
            Vector2 enemyPosition = transform.position;


            Vector2 direction = new Vector2(Mathf.Round(playerPosition.x - enemyPosition.x), Mathf.Round(playerPosition.y - enemyPosition.y));


            transform.position = Vector2.MoveTowards(enemyPosition, enemyPosition + direction, speed * Time.deltaTime);

            if (Vector2.Distance(enemyPosition, enemyPosition + direction) < 0.01f)
            {
                moving = false;
            }
        }
    }
}
