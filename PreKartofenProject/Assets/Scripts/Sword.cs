using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : WeaponClass
{
    private BoxCollider2D collider;
    // Start is called before the first frame update
    void Start()
    {
        damage = 8;
        collider = objectColliders[0].GetComponent<BoxCollider2D>();
    }
}
