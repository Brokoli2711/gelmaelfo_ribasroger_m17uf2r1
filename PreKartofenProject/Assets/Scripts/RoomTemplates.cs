using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTemplates : MonoBehaviour
{
    public GameObject[] bottomRooms;
    public GameObject[] topRooms;
    public GameObject[] leftRooms;
    public GameObject[] rightRooms;

    public GameObject closedRoom;

    public List<GameObject> rooms;

    public GameObject hatch;
    public GameObject[] enemySpawner;

    private void Start()
    {
        Invoke("SpawnHatch",3f);
        Invoke("SpawnEnemySpawner", 2f);
    }

    void SpawnHatch()
    {
        //Aqu� instanciamos la trampilla para pasar de nivel.
        Instantiate(hatch, rooms[rooms.Count - 1].transform.position,Quaternion.identity);
    }

    void SpawnEnemySpawner()
    {
        for(int i = 1; i < rooms.Count - 1; i++) {
            Instantiate(enemySpawner[Random.Range(0,enemySpawner.Length)], rooms[i].transform.position, Quaternion.identity);

        }
    }
}
