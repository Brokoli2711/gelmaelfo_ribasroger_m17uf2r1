using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        BoxCollider2D boxCollider2D = GetComponent<BoxCollider2D>();
    }
    private void Update()
    {
        Die();
    }
    // Update is called once per frame
    private void OnParticleCollision(GameObject other)
    {
        this.life -= 1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Weapon"))
        {
            int damage = collision.gameObject.GetComponentInParent<WeaponClass>().damage;
            life -= damage;

        }
    }


    public void Die()
    {
        if (life <= 0) Destroy(gameObject);
    }
}
