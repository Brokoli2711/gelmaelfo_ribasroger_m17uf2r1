using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomSpawner : MonoBehaviour
{
    public int openSide;

    //1 Need Bottom door
    //2 Need Top door
    //3 Need Left door
    //4 Need Right door
    private int random;
    private RoomTemplates templates;
    private bool spawned = false;

    void Start()
    {
        templates= GameObject.FindGameObjectWithTag("Rooms").GetComponent<RoomTemplates>();
        Invoke("Spawn", 0.1f);
    }


    public void Spawn()
    {
        if(!spawned) {

            if(openSide == 1)
            {
                random = Random.Range(0,templates.topRooms.Length);
                Instantiate(templates.topRooms[random], transform.position, templates.topRooms[random].transform.rotation);
                //Need Top door
            }
            else if (openSide == 2)
            {
                random = Random.Range(0, templates.bottomRooms.Length);
                Instantiate(templates.bottomRooms[random], transform.position, templates.bottomRooms[random].transform.rotation);
                //Need Bottom door
            }
            else if (openSide == 3)
            {
                random = Random.Range(0, templates.leftRooms.Length);
                Instantiate(templates.leftRooms[random], transform.position, templates.leftRooms[random].transform.rotation);
                //Need Left door
            }
            else if (openSide == 4)
            {
                random = Random.Range(0, templates.rightRooms.Length);
                Instantiate(templates.rightRooms[random], transform.position, templates.rightRooms[random].transform.rotation);
                //Need Right door
            }
            spawned = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("SpawnPoint"))
        {
            //if (collision.GetComponent<RoomSpawner>().spawned == false && spawned == false)
            //{
            //    Instantiate(templates.closedRoom, transform.position, Quaternion.identity);
            //    Destroy(gameObject);
            //}
            //spawned = true;
            Destroy(gameObject);

        }
    }
}
