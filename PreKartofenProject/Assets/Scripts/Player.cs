using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Player : Character, IMovement
{
    [SerializeField] private float speed;
    public Vector2 puntoMovimiento;
    [SerializeField] LayerMask obstacles;
    [SerializeField] private Vector2 offSetPuntoMovimiento;
    [SerializeField] private float radioCirculo;
    private bool moving = false;
    private Vector2 input;
    
    void Start()
    {
        puntoMovimiento = transform.position;
        this.rb2d = GetComponent<Rigidbody2D>();
        this._anim= GetComponent<Animator>();
       // weapons[0].Get
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyUp(KeyCode.RightArrow)) rb2d.transform.position = new Vector2(rb2d.position.x + 1, rb2d.position.y);
        //if (Input.GetKeyUp(KeyCode.LeftArrow)) rb2d.transform.position = new Vector2(rb2d.position.x - 1, rb2d.position.y);
        //if (Input.GetKeyUp(KeyCode.UpArrow)) rb2d.transform.position = new Vector2(rb2d.position.x, rb2d.position.y + 1);
        //if (Input.GetKeyUp(KeyCode.DownArrow)) rb2d.transform.position = new Vector2(rb2d.position.x, rb2d.position.y - 1);
        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");
        Move();

        
    }
    private void OnDrawGizmos()
    {   

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(puntoMovimiento + offSetPuntoMovimiento, radioCirculo);
    }

    public void Move()
    {
        if (moving)
        {
            transform.position = Vector2.MoveTowards(transform.position, puntoMovimiento, speed * Time.deltaTime);

            if (Vector2.Distance(transform.position, puntoMovimiento) == 0)
            {
                moving = false;
            }
        }

        if ((input.x != 0 || input.y != 0) && !moving)
        {
            Vector2 pointToProve = new Vector2(transform.position.x, transform.position.y) + offSetPuntoMovimiento + input;
            if (!Physics2D.OverlapCircle(pointToProve, radioCirculo, obstacles))
            {
                moving = true;
                puntoMovimiento += input;
            }
        }
    }

    public void Attack()
    {
        if (!moving && Input.GetKey(KeyCode.W))
        {
            
        }
    }
}
