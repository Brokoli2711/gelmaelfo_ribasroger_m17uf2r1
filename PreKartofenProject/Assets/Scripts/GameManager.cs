using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Player character;
    private Camera camera;
    public GameObject spawn;

    void Start()
    {
        camera = GetComponent<Camera>();
        character.puntoMovimiento = spawn.transform.position;
        character.transform.position = spawn.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
