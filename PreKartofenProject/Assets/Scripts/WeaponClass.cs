using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class WeaponClass : MonoBehaviour, IWeapon
{
    public int damage;
    public GameObject[] objectColliders;

    public void Start()
    {
        for (int i = 0; i < objectColliders.Length; i++)
        {
            objectColliders[i].gameObject.SetActive(false);
        }
    }
    IEnumerator DisableBoxColl()
    {
        Debug.Log("Entramos en la corutina");

        for (int i = 0; i<objectColliders.Length;i++)
        {
            Debug.Log("Activamos WeaponCollider");
            objectColliders[i].gameObject.SetActive(true);
        }
        yield return new WaitForSeconds(0.2f);

        
        for (int i = 0; i < objectColliders.Length; i++)
        {
            Debug.Log("Desactivamos WeaponCollider");

            objectColliders[i].gameObject.SetActive(false);
        }
    }
    
    public void Attack()
    {
        Debug.Log("Atacando");
        StartCoroutine(DisableBoxColl());
        Debug.Log("Hecho");
    }

}


