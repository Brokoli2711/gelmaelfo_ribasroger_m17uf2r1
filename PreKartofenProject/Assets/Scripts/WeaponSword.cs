using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WeaponSword : MonoBehaviour
{

    public int damage;
    public KeyCode position;
    [SerializeField] Rigidbody2D rb2d;
    [SerializeField] Animator _anim;
    void Start()
    {
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(position)) Attack();
    }

    public void Attack()
    {
        
        rb2d.position = new Vector2(rb2d.position.x + 1, rb2d.position.y + 1);
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x, transform.position.y - 3), 5 * Time.deltaTime);
        

    }
}
