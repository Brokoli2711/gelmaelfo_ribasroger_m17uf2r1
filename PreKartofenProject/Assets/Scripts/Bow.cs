using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Bow : WeaponClass
{
    private BoxCollider2D collider;
    // Start is called before the first frame update
    void Start()
    {
        damage = 5;
        collider = objectColliders[0].GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
