using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public void StartScene()
    {
        SceneManager.LoadScene(0);
    }

    public void GameScene()
    {
        SceneManager.LoadScene(1);
    } 

    public void FinalScene()
    {
        SceneManager.LoadScene(2);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
