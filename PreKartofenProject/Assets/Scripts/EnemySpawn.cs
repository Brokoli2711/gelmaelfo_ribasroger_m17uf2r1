using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject[] enemies;

    private void Start()
    {
        foreach(GameObject enemy in enemies)
        {
            enemy.SetActive(false);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision);
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("hola");
            foreach (GameObject e in enemies)
            {
                e.SetActive(true);
            }
        }
    }
}
