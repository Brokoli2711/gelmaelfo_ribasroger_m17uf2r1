using System.Collections;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    private Rocket rocket;
    private Bow bow;
    private Sword sword;
    private FlameThrower flameThrower;

    void Awake()
    {
        rocket = FindObjectOfType<Rocket>();
        bow = FindObjectOfType<Bow>();
        sword = FindObjectOfType<Sword>();
        flameThrower = FindObjectOfType<FlameThrower>();
        if (rocket == null)
        {
            Debug.LogError("No se encontr� un objeto Rocket en la escena.");
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (rocket != null)
            {
                rocket.Attack();
            }
            else
            {
                Debug.LogWarning("El objeto Rocket es nulo en InventoryManager.");
            }
        }
        if(Input.GetKey(KeyCode.A)) bow.Attack();
        if(Input.GetKey(KeyCode.D)) sword.Attack();
        if (Input.GetKey(KeyCode.S)) flameThrower.Atack();
    }
}