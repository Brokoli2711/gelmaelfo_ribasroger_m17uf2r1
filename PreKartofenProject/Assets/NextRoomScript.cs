using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextRoomScript : MonoBehaviour
{
    public Vector2 movement;
    private Camera m_Camera;
    public GameObject playerSpawn;
    private Player player;
    // Start is called before the first frame update
    private void Start()
    {
        m_Camera = GameObject.FindAnyObjectByType<Camera>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            m_Camera.transform.position = new Vector3(m_Camera.transform.position.x + movement.x, m_Camera.transform.position.y + movement.y, -10);
            player.puntoMovimiento = playerSpawn.transform.position;
            player.transform.position = playerSpawn.transform.position;
        }
    }
}
