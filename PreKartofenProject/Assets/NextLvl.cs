using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLvl : MonoBehaviour
{
    private UIController _controller;
    private void Start()
    {
        _controller = new UIController();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) _controller.FinalScene();
    }
}
